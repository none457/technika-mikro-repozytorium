# Termometr LCD z Arduino i LM35

## Cele do realizacji
- [x]  Podświetlenie ekranu w oparciu o rezystor nastawny
- [x]  Poprawne wyświetlanie komunikatów ekranie LCD
- [x]  Rejestracja temperatury
- [x]  Wyświetlanie wartości ułamkowych

## Użyte elementy
1. Analogowy czujnik temperatury LM35
2. Arduino Uno z uC Atmega328P
3. Wyświetlacz znakowy LCD HD44780
4. Potencjometr montażowy leżący RM065 10kohm
5. Płytka połączeniowa
6. Przewody

## Elementy wymagające zakupu:
- [x] LM35 – moduł mostka
- [x] Arduino Uno
- [x] Wyświetlacz znakowy LCD HD44780

## Środowisko programistyczne:
1. Arduino IDE

# Wersja 1
## Działanie układu.

<b>Pierwsze uruchomienie.</b><br>
- Podłączenie wyświetlacza i sprawdzenie czy działa poprawnie.<br>
- Wyświetlenie jakiegoś znaku i czyszczenie po czasie.<br>
<p></p>


![](./zdjecia/2.jpg)
<p></p>
<p></p><br>

## Stan kodu:
1. Działa tylko wyświetlacz.

## Kolejne etapy:
1. Podłączenie LM35
2. Testy.

# Wersja 2

## Działanie układu.
Dodano:
- LM35 <br><p></p>


![](./zdjecia/1.jpg)

<p></p>
<p></p>


